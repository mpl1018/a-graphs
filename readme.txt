Para compilar:
	make

Para ejecutar:
	./program.exe


Si se quieren ejecutar determinados tests hay que descomentar las llamadas
que interesen en el main del A_graphs_percolation_nodes_or_edges.cc

Los resultados generados y estudiados en el documento Conectividad_y_percolación.pdf
los hemos guardado en las carpetas /Tests_conectividad y /Tests_numero_ccs
y se pueden visualizar de manera ordenada en el Conectividad_y_percolación_Anexo.pdf.

Los archivos han estado generados simplemente con el uso del operador de redirecció (>) desde la terminal.
Las gráficas han estado generadas con "https://www.chartgo.com/" y con Excel.